# ukfast/pokédex

**This project is based primarily on my ability to fulfill the task 
requirements. Any potential design skills are a bonus, but usability, 
performance and security will be taken into account.**

## Introduction
This project provides a starting point which allowed to create my own 
web-based encyclopedia based on the popular franchise Pokémon - also known as 
a pokédex.

## Project Requirements
To get started, I needed the following:

 - PHP
 - [Composer](https://getcomposer.org/)
 - git
 - docker
 
 I was free to use whatever PHP packages and front-end libraries that I
 wish.

## Task Requirements
To order to complete this challenge, I created a pokédex with minimal 
functionality. The solutions allows the user to browse the full list of 
pokémon in a convenient manner, as well as offer some form of search 
functionality.The solution also displays basic information for a 
specific pokémon, including:

 - At least one image of the pokémon
 - Name
 - Species
 - Height and weight
 - Abilities
 
A RESTful API is available at [Pokéapi](https://pokeapi.co/) which provides majority of the data that I wanted. I did not need to create 
an account nor authenticate in order to consume the API, however please 
be aware that this API is rate-limited.
 
It has taken me 4 hours of work, including setting up environment from scratch, to get to the point of having full database, models and the front sorted.
The only thing I did not write myself was the pokedex image in css, which I found <a href="https://codepen.io/fizzypop109/pen/gOYXPXE?&page=1">here.</a>

Unfortunately, I do not feel that redoing the pokedex like the one presented would be sorted within 3 hours of my time, as my knowledge of CSS is not that good to sort that quicker.

If anyone is curious what took me most time, I got stuck in a while loop, because I forgot to increase the integer to get to next step in case of failure x)

## Copyright
All trademarks as the property of their respective owners.

