@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="pokedex">

                <div class="pokedex-left">

                    <div class="pokedex-lights-container">
                        <div class="pokedex-lights-lg">
                        </div>
                        <div class="pokedex-lights-sm-container">
                            <div class="pokedex-lights-sm-light one">
                            </div>
                            <div class="pokedex-lights-sm-light two">
                            </div>
                            <div class="pokedex-lights-sm-light three">
                            </div>
                        </div>
                    </div>

                    <div class="pokedex-screen-cut">
                    </div>

                    <div class="pokedex-screen">

                        <div class="pokedex-screen-top">
                            <div class="pokedex-screen-light">
                            </div>
                            <div class="pokedex-screen-light">
                            </div>
                        </div>

                        <div class="pokedex-screen-image-container">
                            <img class="pokedex-screen-image" src="{{$pokemon->sprite}}">
                        </div>

                        <div class="pokedex-screen-bottom">
                            <div class="pokedex-screen-button">
                            </div>
                            <div class="pokedex-screen-vents">
                                <div class="pokedex-screen-vent">
                                </div>
                                <div class="pokedex-screen-vent">
                                </div>
                                <div class="pokedex-screen-vent">
                                </div>
                                <div class="pokedex-screen-vent">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="pokedex-controls">
                        <div class="pokedex-controls-button">
                        </div>
                        <div class="pokedex-controls-longButton redButton">
                        </div>
                        <div class="pokedex-controls-longButton blueButton">
                        </div>
                    </div>

                    <div class="pokedex-pad">
                        <div class="pokedex-pad-middle">
                            <div class="pokedex-pad-middle-circle">
                            </div>
                        </div>
                        <div class="pokedex-pad-v">
                        </div>
                        <div class="pokedex-pad-h">
                        </div>
                    </div>

                    <div class="pokedex-smallScreen text-center">
                        {{ucfirst($pokemon->species)}}
                    </div>

                </div>

                <div class="pokedex-right">


                    <div class="pokedex-info-container">
                        <div>Height:
                            @if($pokemon->height > 9)
                                {{$pokemon->height[0]}} m {{$pokemon->height[1]}} dm
                            @else
                                {{$pokemon->height[0]}} dm
                            @endif
                        </div>
                        {{--                        Could make it shorter by parsing "m {{$pokemon->height[1]}}" inside an if, but this way is more readable--}}
                        <div>Weight: {{($pokemon->height)/10}} kg</div>

                        @foreach($pokemon->abilities as $ability)
                            <div data-toggle="tooltip" data-placement="right"
                                 title="{{$ability->description}}">{{ucfirst($ability->name)}}</div>
                        @endforeach


                    </div>

                    <div class="pokedex-buttons-shadow">
                    </div>

                    <div class="pokedex-buttons">
                        <div class="pokedex-button first">
                        </div>
                        <div class="pokedex-button">
                        </div>
                        <div class="pokedex-button">
                        </div>
                        <div class="pokedex-button">
                        </div>
                        <div class="pokedex-button fifth">
                        </div>
                        <div class="pokedex-button sixth">
                        </div>
                        <div class="pokedex-button">
                        </div>
                        <div class="pokedex-button">
                        </div>
                        <div class="pokedex-button">
                        </div>
                        <div class="pokedex-button tenth">
                        </div>
                    </div>

                    <div class="pokedex-longButtons">
                        <div class="pokedex-longButton">
                        </div>
                        <div class="pokedex-longButton">
                        </div>
                    </div>

                    <div class="pokedex-dualButtons-shadow">
                    </div>

                    <div class="pokedex-dualButtons">
                        <div class="pokedex-dualButton first">
                        </div>
                        <div class="pokedex-dualButton second">
                        </div>
                    </div>

                    <div class="pokedex-goldButton">
                    </div>

                    <div class="pokedex-blackButtons">
                        <button class="btn pokedex-blackButton"
                                onclick="window.location.href='/show/{{$pokemon->pokedex_id - 1}}'">
                            <- Previous
                        </button>
                        <button class="btn pokedex-blackButton"
                                onclick="window.location.href='/show/{{$pokemon->pokedex_id + 1}}'">
                            Next ->
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
