@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-md-10 col-sm-12">
                <div class="card" style="background-color: #880000">

                    <div class="card-header"><p>Hello there!</p>
                        <p style="opacity: 0">General Kenobi!</p>
                    </div>
                    <div class="card-body">
                        <table id="pokemontable"
                               class="table table-bordered table-striped table-hover justify-content-center">
                            <thead>
                                <tr>
                                    <th>Pokedex ID</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(App\Pokemon::all() as $pokemon)
                                    <tr>
                                        <td><a href="/show/{{$pokemon->pokedex_id}}">{{$pokemon->pokedex_id}}</a></td>
                                        <td><a href="/show/{{$pokemon->pokedex_id}}">{{ucfirst($pokemon->species)}}</a>
                                        </td>
                                        <td><img src="{{$pokemon->sprite}}"></td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Pokedex ID</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <script>

            $('#pokemontable').DataTable();


    </script>
@endsection
