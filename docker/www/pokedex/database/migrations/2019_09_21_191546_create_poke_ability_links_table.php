<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokeAbilityLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('poke_ability_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pokemon_id');
            $table->bigInteger('ability_id');
        });
    }
}
