<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokemonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pokemons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pokedex_id');
            $table->string('sprite');
            $table->string('species');
            $table->string('height');
            $table->string('weight');
        });
    }
}
