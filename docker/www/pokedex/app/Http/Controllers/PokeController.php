<?php

namespace App\Http\Controllers;

use App\Ability;
use App\PokeAbilityLink;
use App\Pokemon;
use Illuminate\Http\Request;

class PokeController extends Controller
{

    public function show($id)
    {
        $pokemon = Pokemon::where('pokedex_id', $id)->first();
        if (!$pokemon) {
            return view('welcome');
        }
        return view('pokemonshow', ['pokemon' => $pokemon]);
    }
    public function home(){
        return view('welcome');
    }

    public function pull()
    {
        Pokemon::pullAll();
        Ability::pullAll();
    }
}
