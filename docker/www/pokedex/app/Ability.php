<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PokePHP\PokeApi;

class Ability extends Model
{
    public $timestamps = false;
    public $guarded = [];

    public static function pull($id) //
    {
        $api = new PokeApi();
        try {
            $abilitydata = json_decode($api->ability($id));
        } catch (\Exception $e) {
            throw new \Exception('Cannot download ability data for: ' . $id);
        }

        $ability = Ability::firstOrCreate(['name' => $abilitydata->name], ['description' => $abilitydata->effect_entries[0]->effect]);
        foreach ($abilitydata->pokemon as $key => $value) {
            $pokemon = Pokemon::where('species', $value->pokemon->name)->first();
            if ($pokemon) { // just a check if the pokemon exists
                PokeAbilityLink::firstOrCreate(['pokemon_id' => $pokemon->pokedex_id, 'ability_id' => $ability->id]);
            } else {
                throw new \Exception('This pokemon does not exist: ' . $value->pokemon->name, 1);
            }
        }
    }

    public static function pullAll()
    {
        $id = 9;
        while ($id) {
            if (!static::find($id)) {

                try {
                    static::pull($id);
                    $id++;
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    if ($e->getCode() === 0) {
                        print_r('End of list');
                        exit;
                    }
                    if ($e->getCode() === 1) {
                        print_r('Contact the administrator');
                        foreach(['alola','totem','-'] as $value) {
                            if (strpos($e->getMessage(), $value)) {
                                continue 2;
                            }
                        }
                        exit;
                    }
                }
            } else{
                $id++;
            }
        }
    }
    public function pokemons(){
        return $this->belongsToMany(Pokemon::class, 'poke_ability_links', 'ability', 'pokemon_id', 'id', 'pokedex_id');
    }
}
