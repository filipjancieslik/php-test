<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PokePHP\PokeApi;

class Pokemon extends Model
{
    public $table = 'pokemons'; //stupid laravel does not know how to make it plural
    public $timestamps = false;
    protected $guarded = [];

    public static function pull($id)
    {

        $api = new PokeApi();
        try {
            $pokemondata = json_decode($api->pokemon($id));
        } catch (\Exception $e) {
            throw new \Exception('Cannot download pokemon data for: ' . $id, 1); // either connection error or end of list
        }

        if (Pokemon::firstOrCreate(['pokedex_id' => $pokemondata->id],
            ['sprite' => $pokemondata->sprites->front_default
                , 'species' => $pokemondata->name
                , 'weight' => $pokemondata->weight
                , 'height' => $pokemondata->height
            ])) {
            return true; //created new one
        }
        return false;

    }

    public static function pullAll()
    {
        $id = 1;
        while ($id) {
            if (!static::where('pokedex_id', $id)->first()) {
                try {
                    static::pull($id);
                    $id++;
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    if ($e->getCode() === 1) {
                        break;
                    }
                }
            } else {
                $id++;
            }
        }
        return true;
    }

    public function abilities()
    {

        return $this->belongsToMany(Ability::class, 'poke_ability_links', 'pokemon_id', 'ability_id', 'id', 'id');
    }
}
