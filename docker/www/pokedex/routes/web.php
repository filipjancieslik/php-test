<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PokeController;

Route::get('/', 'PokeController@home')->name('home');
Route::get('/show/{id}','PokeController@show');
Route::get('/pull/','PokeController@pull');

//Auth::routes(); // not needed
